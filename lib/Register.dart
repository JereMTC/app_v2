import 'package:flutter/material.dart';
import 'login.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Formregistro();
  }
}

class Formregistro extends StatefulWidget {
  @override
  _FormregistroState createState() => _FormregistroState();
}

class _FormregistroState extends State<Formregistro> {
  @override
  var _vista = '¿Quien va a usar la cuenta?';
  String _anio = '¿Año en cursado?';
  String _division = '¿Divisíon?';
  String _bajotutela = '¿cantida de Estudiantes vajo su tutela?';
  String _institucion = '¿Institución?';

  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        //-----------------------------------------------------------padding
        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
        children: [
          Column(
            children: <Widget>[
              //-----------------------------------------------Solicitud de Cuenta
              Center(
                child: Text(
                  "Solicitud de Cuenta",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              SizedBox(height: 50),
              //-----------------------------------------------------------Nombre
              TextFormField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Nombre'),
                ),
              ),
              SizedBox(height: 20),
              //-------------------------------------------------------Apellido
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Apellido'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              //--------------------------------------------------Fecha Nacimiento
              TextFormField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Fecha de nacimiento'),
                ),
              ),
              SizedBox(height: 20),
              //---------------------------------------------------------------DNI
              TextFormField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('DNI'),
                ),
              ),
              SizedBox(height: 40),

              //-----------------------------------------------------------Gmail
              TextFormField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Gmail'),
                ),
              ),
              SizedBox(height: 20),
              //-------------------------------------------------------Contraseña
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Contraseña'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              //----------------------------------------comfirmación de contraseña
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Comfirmación Contraseña'),
                ),
              ),
              SizedBox(height: 20),

              //----------------------------------------------------NRO Telefono
              TextFormField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ('Nro Telefono'),
                ),
              ),
              //-----------------------------------------------Boton desplegable
              DropdownButton(
                items: <String>['Preseptor', 'Tutor legal']
                    .map<DropdownMenuItem<String>>((String valor) {
                  return DropdownMenuItem<String>(
                    value: valor,
                    child: Text(valor),
                  );
                }).toList(),
                elevation: 23,
                underline: Container(
                  height: 3,
                  color: Colors.blue,
                ),
                onChanged: (String? valueIn) {
                  setState(() {
                    _vista = valueIn!;
                  });
                },
                hint: Text(_vista),
              ),
              Column(children: [
                DropdownButton(
                  items: <String>['1,2,3,4,5']
                      .map<DropdownMenuItem<String>>((String valor) {
                    return DropdownMenuItem<String>(
                      value: valor,
                      child: Text(valor),
                    );
                  }).toList(),
                  elevation: 23,
                  underline: Container(
                    height: 3,
                    color: Colors.blue,
                  ),
                  onChanged: (String? valueIn) {
                    setState(() {
                      _bajotutela = valueIn!;
                    });
                  },
                  hint: Text(_bajotutela),
                ),
                Column(
                  children: [
                    //-----------------------------------------------------Nombre Estudiante
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: ('Nombre del Estudiante'),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    //---------------------------------------------------Apellido estudiante
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: ('Apellido de estudiante'),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    //--------------------------------------------------------DNI estudiante
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: ('DNI del Estudiante'),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    DropdownButton(
                      items: <String>['1,2,3,4,5,6']
                          .map<DropdownMenuItem<String>>((String valor) {
                        return DropdownMenuItem<String>(
                          value: valor,
                          child: Text(valor),
                        );
                      }).toList(),
                      elevation: 23,
                      underline: Container(
                        height: 3,
                        color: Colors.blue,
                      ),
                      onChanged: (String? valueIn) {
                        setState(() {
                          _institucion = valueIn!;
                        });
                      },
                      hint: Text(_institucion),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    //-------------------------------------------------------------------Año
                    DropdownButton(
                      items: <String>['1,2,3,4,5,6']
                          .map<DropdownMenuItem<String>>((String valor) {
                        return DropdownMenuItem<String>(
                          value: valor,
                          child: Text(valor),
                        );
                      }).toList(),
                      elevation: 23,
                      underline: Container(
                        height: 3,
                        color: Colors.blue,
                      ),
                      onChanged: (String? valueIn) {
                        setState(() {
                          _anio = valueIn!;
                        });
                      },
                      hint: Text(_anio),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    //division
                    DropdownButton(
                      items: <String>['1ra,2da,3ra,4ta,5ta,6ta']
                          .map<DropdownMenuItem<String>>((String valor) {
                        return DropdownMenuItem<String>(
                          value: valor,
                          child: Text(valor),
                        );
                      }).toList(),
                      elevation: 23,
                      underline: Container(
                        height: 3,
                        color: Colors.blue,
                      ),
                      onChanged: (String? valueIn) {
                        setState(() {
                          _division = valueIn!;
                        });
                      },
                      hint: Text(_division),
                    ),
                  ],
                ),
              ]),

              //------------------------------------------------------------Enviar
              ElevatedButton(child: Text("  Enviar  "), onPressed: () => {}),

              //----------------------------------------------------Iniciar Sesion
              TextButton(
                  onPressed: () => {Navigator.pop(context)},
                  child:
                      Text("Iniciar Sesión", style: TextStyle(fontSize: 10))),
              SizedBox(
                height: 2,
              ),
              //-----------------------------------------------------------Soporte
              TextButton(
                  onPressed: () => {},
                  child: Text("Soporte", style: TextStyle(fontSize: 10))),
            ],
          )
        ],
      ),
    );
  }
}

class RegisterFinalizado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PantallaRegistroTres(),
    );
  }
}

class Preseptor extends StatefulWidget {
  _PreseptorState createState() => _PreseptorState();
}

class _PreseptorState extends State<Preseptor> {
  String _vista = '¿Quien va a usar la cuenta?';
  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      items: <String>['Preseptor', 'Tutor legal']
          .map<DropdownMenuItem<String>>((String valor) {
        return DropdownMenuItem<String>(
          value: valor,
          child: Text(valor),
        );
      }).toList(),
      elevation: 23,
      underline: Container(
        height: 3,
        color: Colors.blue,
      ),
      onChanged: (String? valueIn) {
        setState(() {
          _vista = valueIn!;
        });
      },
      hint: Text(_vista),
    );
  }
}

class PantallaRegistroTres extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      //-----------------------------------------------------------padding
      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
      child: Column(
        children: <Widget>[
          //------------------------------------msg Solicitud de Cuenta envida
          Center(
            child: Text(
              "Su Solicitud de cuenta se a Enviado correctamente",
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(height: 20),
          //-----------------------------------------------------------msg
          Text(
            "su cuenta sera validada proximamente",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(height: 100),
          //---------------------------------------------------Boton Finalizar
          ElevatedButton(
            child: Text("  Finalizar  "),
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Login()))
            },
          ),
        ],
      ),
    );
  }
}
