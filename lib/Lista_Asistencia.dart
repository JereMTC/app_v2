import 'package:flutter/material.dart';

class Listado extends StatefulWidget {
  @override
  _ListadoState createState() => _ListadoState();
}

class _ListadoState extends State<Listado> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: DataTable(
      sortColumnIndex: 2,
      sortAscending: false,
      columns: [
        DataColumn(label: Text("Nombre")),
        DataColumn(label: Text("Apellido")),
        DataColumn(label: Text("Años"), numeric: true),
      ],
      rows: [
        DataRow(selected: true, cells: [
          DataCell(Text("Andres"), showEditIcon: true),
          DataCell(Text("Cruz")),
          DataCell(Text("28"))
        ]),
        DataRow(cells: [
          DataCell(Text("Ramos")),
          DataCell(Text("Ayu")),
          DataCell(Text("999"))
        ])
      ],
    ));
  }
}
