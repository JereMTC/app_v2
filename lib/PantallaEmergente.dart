import 'package:flutter/material.dart';

class P_Emergente extends StatefulWidget {
  static String id = 'Pantallaemergente';

  @override
  _P_EmergenteState createState() => _P_EmergenteState();
}

class _P_EmergenteState extends State<P_Emergente> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          "Principa",
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            //--------------------------------------------------------------Menu
            Expanded(
              child: Text(
                "Menu",
                style: TextStyle(backgroundColor: Colors.amber),
              ), //poner una imagen del usuario
              //child: Image(image: image),
            ),
            SizedBox(height: 2),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Perfil"),
              onTap: () {},
            ),
            SizedBox(height: 5),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Planillas"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Informes"),
              onTap: () {},
            ),
            SizedBox(height: 5),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Notificaciones"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Contactos"),
              onTap: () {},
            ),
            SizedBox(height: 5),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Soporte"),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Salir"),
              onTap: () {},
            ),
          ],
        ),
      ),
    ));
  }
}
