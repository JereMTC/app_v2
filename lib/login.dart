import 'package:app_flutter_v2/Lista_Asistencia.dart';
import 'package:app_flutter_v2/PantallaEmergente.dart';
import 'package:app_flutter_v2/Register.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PantallaLogin(),
    );
  }
}

class PantallaLogin extends StatelessWidget {
  final _keyform = GlobalKey<FormState>();
  final requiredValidator = MultiValidator([
    RequiredValidator(errorText: 'Ingrese una direccion de correo electronio'),
    EmailValidator(
        errorText: 'Introduzca una dirección de correo electrónico válida')
  ]);
  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'Ingrese una contraseña '),
  ]);
  late String password;
  late String gmail;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _keyform,
      child: Container(
        //-----------------------------------------------------------padding
        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
        child: Column(
          children: <Widget>[
            //-----------------------------------------------Inicio de sesión
            Center(
              child: Text(
                "Inicio de Sesión",
                style: TextStyle(fontSize: 30),
              ),
            ),
            SizedBox(height: 50),
            //-----------------------------------------------------------Gmail
            TextFormField(
              validator: requiredValidator,
              obscureText: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: ('Gmail'),
                prefixIcon: Icon(Icons.email_outlined),
              ),
              onChanged: (val) => gmail = val,
            ),

            SizedBox(height: 30),
            //-------------------------------------------------------Contraseña
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: ('Password'),
                prefixIcon: Icon(Icons.lock_outline),
              ),
              onChanged: (val) => password = val,
              validator: passwordValidator,
            ),
            SizedBox(
              height: 5,
            ),
            //-----------------------------------------¿olvidaste tu contraseña?
            TextButton(
                onPressed: () => {},
                child: Text("¿Olvidaste tu contraseña?",
                    style: TextStyle(fontSize: 10))),
            SizedBox(
              height: 20,
            ),
            //-----------------------------------------------Boton iniciar sesio
            ElevatedButton(
              child: Text("  Iniciar Sesión  "),
              onPressed: () => {
                if (_keyform.currentState!.validate())
                  {
                    print("Validacion exitosa"),
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => P_Emergente()))
                  }
                else
                  {print("A ocurrido un error")},
              },
            ),
            SizedBox(
              height: 200,
            ),
            //--------------------------------------------------Solicitar Cuenta
            TextButton(
                onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Register())),
                    },
                child:
                    Text("Solicitar Cuenta", style: TextStyle(fontSize: 10))),
            SizedBox(
              height: 2,
            ),
            //-----------------------------------------------------------Soporte
            TextButton(
                onPressed: () => {},
                child: Text("Soporte", style: TextStyle(fontSize: 10))),
          ],
        ),
      ),
    );
  }
}
